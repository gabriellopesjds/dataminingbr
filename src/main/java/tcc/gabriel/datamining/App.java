package tcc.gabriel.datamining;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import tcc.gabriel.datamining.sentiment.SenticNet;

@SpringBootApplication
public class App implements CommandLineRunner{
	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}

	@Override
	public void run(String... arg0) throws Exception {
	}
}